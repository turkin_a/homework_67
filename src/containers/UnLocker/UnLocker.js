import React, {Component} from 'react';
import {connect} from 'react-redux';
import './UnLocker.css';

import Button from "../../components/UI/Button/Button";

class UnLocker extends Component {
  screenContent = () => {
    if (this.props.accessIs) return 'Access ' + this.props.accessIs;
    return Array(this.props.userCode.length + 1).join('*');
  };

  render() {
    let screenClass = '';
    if (this.props.accessIs) screenClass = 'Access-' + this.props.accessIs;

    let buttons = [];
    for (let i = 3; i >= 1; i--) {
      for (let j = 2; j >= 0; j--) {
        buttons.push(<Button key={i * 3 - j} clicked={() => this.props.inputNumber(i * 3 - j)}>{i * 3 - j}</Button>);
      }
    }

    return (
      <div className="UnLocker">
        <div className={`UnLocker-screen ${screenClass}`}>
          {this.screenContent()}
        </div>
        <div className="UnLocker-keyboard">
          {buttons.map(button => button)}
          <Button clicked={this.props.inputRemove}>{'<'}</Button>
          <Button clicked={() => this.props.inputNumber(0)}>0</Button>
          <Button clicked={this.props.inputEnter}>E</Button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    userCode: state.userCode,
    accessIs: state.accessIs
  }
};

const mapDispatchToProps = dispatch => {
  return {
    inputNumber: (value) => dispatch({type: 'INPUT_NUMBER', value}),
    inputRemove: () => dispatch({type: 'REMOVE_NUMBER'}),
    inputEnter: () => dispatch({type: 'CONFIRM_CODE'})
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(UnLocker);