import React, {Component} from 'react';
import {connect} from 'react-redux';
import './Calculator.css';

import Button from "../../components/UI/Button/Button";

class Calculator extends Component {
  render() {
    let calcClassName = '';
    if (this.props.calcIsMemory) calcClassName = ' Visible';

    return (
      <div className="Calculator">
        <div className="Calculator-screen">
          <span className={"Calculator-memory" + calcClassName}>M</span>
          {this.props.calcCurrentValue}
        </div>
        <div className="Calculator-keyboard">
          <Button clicked={this.props.calcClear}>C</Button>
          <Button clicked={this.props.memoryClear}>CM</Button>
          <Button clicked={this.props.memoryRead}>RM</Button>
          <Button clicked={this.props.memoryIncrease}>M+</Button>
          <Button clicked={this.props.memoryDecrease}>M-</Button>
          <Button clicked={() => this.props.inputNumber('7')}>7</Button>
          <Button clicked={() => this.props.inputNumber('8')}>8</Button>
          <Button clicked={() => this.props.inputNumber('9')}>9</Button>
          <Button clicked={this.props.changeSign}>±</Button>
          <Button clicked={this.props.takeSquareRoot}>√</Button>
          <Button clicked={() => this.props.inputNumber('4')}>4</Button>
          <Button clicked={() => this.props.inputNumber('5')}>5</Button>
          <Button clicked={() => this.props.inputNumber('6')}>6</Button>
          <Button clicked={() => this.props.inputOperation('*')}>×</Button>
          <Button clicked={() => this.props.inputOperation('/')}>÷</Button>
          <Button clicked={() => this.props.inputNumber('1')}>1</Button>
          <Button clicked={() => this.props.inputNumber('2')}>2</Button>
          <Button clicked={() => this.props.inputNumber('3')}>3</Button>
          <Button clicked={() => this.props.inputOperation('+')}>+</Button>
          <Button clicked={() => this.props.inputOperation('-')}>−</Button>
          <Button clicked={() => this.props.inputNumber('0')}>0</Button>
          <Button clicked={() => this.props.inputNumber('00')}>00</Button>
          <Button clicked={() => this.props.inputNumber('.')}>.</Button>
          <Button clicked={this.props.removeNumber}>→</Button>
          <Button clicked={this.props.takeResult}>=</Button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    calcCurrentValue: state.calcCurrentValue,
    calcCurrentResult: state.calcCurrentResult,
    calcCurrentMemory: state.calcCurrentMemory,
    calcIsMemory: state.calcIsMemory
  }
};

const mapDispatchToProps = dispatch => {
  return {
    inputNumber: (value) => dispatch({type: 'CALC_NUMBER', value}),
    removeNumber: () => dispatch({type: 'CALC_REMOVE'}),
    inputOperation: (value) => dispatch({type: 'CALC_OPERATION', value}),
    changeSign: () => dispatch({type: 'CALC_SIGN'}),
    takeSquareRoot: () => dispatch({type: 'CALC_SQRT'}),
    takeResult: () => dispatch({type: 'CALC_RESULT'}),
    memoryIncrease: () => dispatch({type: 'CALC_MEMORY_UP'}),
    memoryDecrease: () => dispatch({type: 'CALC_MEMORY_DOWN'}),
    memoryRead: () => dispatch({type: 'CALC_MEMORY_READ'}),
    memoryClear: () => dispatch({type: 'CALC_MEMORY_CLEAR'}),
    calcClear: () => dispatch({type: 'CALC_CLEAR'})
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);