import React from 'react';
import './Toolbar.css';
import {NavLink} from "react-router-dom";

const Toolbar = () => (
  <header className="Toolbar">
    <NavLink className="Toolbar-Item" activeClassName="Selected"
             to='/' exact>Main</NavLink>
    <NavLink className="Toolbar-Item" activeClassName="Selected"
             to='/addition'>Addition</NavLink>
  </header>
);

export default Toolbar;