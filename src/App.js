import React, { Component } from 'react';
import {Switch, Route} from "react-router-dom";

import Layout from "./components/Layout/Layout";
import UnLocker from "./containers/UnLocker/UnLocker";
import Calculator from "./containers/Calculator/Calculator";

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={UnLocker} />
          <Route path="/addition" component={Calculator} />
        </Switch>
      </Layout>
    );
  }
}

export default App;