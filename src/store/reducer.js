/* eslint-disable no-eval */

import actionTypes from "./actionTypes";

const pinCode = '3302';

const initialState = {
  userCode: '',
  accessIs: null,
  calcCurrentValue: '0',
  calcCurrentResult: '',
  calcCurrentMemory: 0,
  calcInputNewValue: true,
  calcIsThisResult: false,
  calcIsFloat: false,
  calcIsMemory: false
};

const reducer = (state = initialState, action) => {
  if (state.calcCurrentValue === 'error') action.type = 'CALC_CLEAR';
  let result;
  let isFloat = false;
  let prevSymbol = '';

  try {
    switch (action.type) {
      case actionTypes.INPUT_NUMBER: {
        if (state.userCode.length === 4) return state;
        return {...state, userCode: state.userCode += String(action.value), accessIs: null};
      }
      case actionTypes.REMOVE_NUMBER:
        return {...state, userCode: state.userCode.slice(0, -1), accessIs: null};
      case actionTypes.CONFIRM_CODE: {
        if (state.userCode === pinCode) {
          return {...state, userCode: '', accessIs: 'Granted'};
        }
        else {
          return {...state, userCode: '', accessIs: 'Denied'};
        }
      }

      case actionTypes.CALC_NUMBER: {
        if (action.value === '.') {
          if (state.calcIsFloat) return state;
          else isFloat = true;
        }
        if (state.calcInputNewValue) {
          if (action.value === '.') return {...state, calcCurrentValue: '0' + action.value, calcInputNewValue: false, calcIsFloat: isFloat};
          else if (action.value === '00') return {...state, calcCurrentValue: '0', calcInputNewValue: false, calcIsFloat: isFloat};
          else return {...state, calcCurrentValue: action.value, calcInputNewValue: false, calcIsFloat: isFloat};
        }
        if (state.calcCurrentValue.length === 20) return state;
        if (state.calcCurrentValue === '0' && action.value !== '.') {
          if (action.value === '00') return {...state, calcCurrentValue: '0', calcInputNewValue: false};
          else return {...state, calcCurrentValue: action.value, calcInputNewValue: false, calcInputNewOperator: true};
        }
        return {...state, calcCurrentValue: state.calcCurrentValue += action.value, calcInputNewValue: false, calcIsFloat: isFloat};
      }
      case actionTypes.CALC_REMOVE:
        if (!state.calcInputNewValue) {
          if ((parseFloat(state.calcCurrentValue) >= 0 && state.calcCurrentValue.length === 1) ||
            (parseFloat(state.calcCurrentValue) < 0 && state.calcCurrentValue.length === 2))
            return {...state, calcCurrentValue: '0'};
          else return {...state, calcCurrentValue: state.calcCurrentValue.slice(0, -1)};
        }
        else return state;
      case actionTypes.CALC_OPERATION:
        prevSymbol = state.calcCurrentResult[state.calcCurrentResult.length - 1];
        result = state.calcCurrentResult;
        if (state.calcInputNewValue && (prevSymbol === '+' || prevSymbol === '-' || prevSymbol === '*' || prevSymbol === '/')) {
          result = result.slice(0, -1) + action.value;
          return {...state, calcCurrentResult: result};
        }
        if (state.calcInputNewValue && state.calcIsThisResult) result = state.calcCurrentValue;
        else result = String(eval(state.calcCurrentResult + state.calcCurrentValue));
        if (result === 'Infinity') throw new SyntaxError('error');
        return {...state, calcCurrentResult: result + action.value, calcCurrentValue: result, calcInputNewValue: true, calcIsThisResult: false, calcIsFloat: false};
      case actionTypes.CALC_SIGN:
        return {...state, calcCurrentValue: String(- state.calcCurrentValue)};
      case actionTypes.CALC_SQRT:
        if (state.calcCurrentValue < 0) throw new SyntaxError('error');
        return {...state, calcCurrentValue: Math.sqrt(parseFloat(state.calcCurrentValue)), calcInputNewValue: true};
      case actionTypes.CALC_RESULT:
        if (state.calcIsThisResult) result = state.calcCurrentValue;
        else result = String(eval(state.calcCurrentResult + state.calcCurrentValue));
        if (result === 'Infinity') throw new SyntaxError('error');
        return {...state, calcCurrentResult: result, calcCurrentValue: result, calcInputNewValue: true, calcIsThisResult: true, calcIsFloat: false};
      case actionTypes.CALC_MEMORY_UP:
        return {...state, calcCurrentMemory: state.calcCurrentMemory + parseFloat(state.calcCurrentValue), calcInputNewValue: true, calcIsMemory: true};
      case actionTypes.CALC_MEMORY_DOWN:
        return {...state, calcCurrentMemory: state.calcCurrentMemory - parseFloat(state.calcCurrentValue), calcInputNewValue: true, calcIsMemory: true};
      case actionTypes.CALC_MEMORY_READ:
        if (state.calcIsMemory) return {...state, calcCurrentValue: String(state.calcCurrentMemory), calcInputNewValue: true};
        else return state;
      case actionTypes.CALC_MEMORY_CLEAR:
        return {...state, calcCurrentMemory: 0, calcIsMemory: false};
      case actionTypes.CALC_CLEAR:
        return {...state, calcCurrentValue: '0', calcCurrentResult: ''};
      default:
        return state;
    }
  } catch (error) {
    return {...state, calcCurrentValue: 'error'};
  }
};

export default reducer;